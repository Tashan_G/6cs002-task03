package c.patterns;

public class GameRemoveADomino implements Game{
	private GameDomino undoplacedomino;
	
	public GameRemoveADomino(GameDomino placedomino) {
		this.undoplacedomino = placedomino;
	}

	@Override
	public void execute() {
		undoplacedomino.unplaceDomino();
	}

}
