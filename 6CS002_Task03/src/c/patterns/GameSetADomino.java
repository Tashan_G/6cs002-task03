package c.patterns;

public class GameSetADomino implements Game{
	private GameDomino doplacedomino;
	
	public GameSetADomino(GameDomino placedomino) {
		this.doplacedomino = placedomino;
	}

	@Override
	public void execute() {
		doplacedomino.placeDomino();
	}
}
