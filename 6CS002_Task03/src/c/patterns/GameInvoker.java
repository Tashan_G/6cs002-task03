package c.patterns;

public class GameInvoker {
	private Game doGame, undoGame;
	public GameInvoker(GameSetADomino doCommand) {
		this.doGame = doCommand;
	}
	
	public GameInvoker(GameSetADomino doCommand, GameRemoveADomino undoCommand) {
		this.doGame = doCommand;
		this.undoGame = undoCommand;
	}
	
	public void setUndoInvoker(GameRemoveADomino undoCommand) {
		this.undoGame = undoCommand;
	}
	
	public void doPlace() {
		doGame.execute();
	}
	
	public void undoPlace() {
		undoGame.execute();
	}

}
